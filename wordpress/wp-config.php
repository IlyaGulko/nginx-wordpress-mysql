<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress');

/** MySQL database username */
define( 'DB_USER', 'root');

/** MySQL database password */
define( 'DB_PASSWORD', 'wordpress');

/** MySQL hostname */
define( 'DB_HOST', 'mysql');

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'QeG|G}9aaxRe{L`l^L5$G82n:JL)&K}`fH&R<9T1T{ir_dG2@H?QPrS0A<lam(H9' );
define( 'SECURE_AUTH_KEY',  'Pn @W|ktWjS+ZjRUbvS6rS4y3rr3$GfygclO$UDH0!.Fvb Z~-:]0[,&-WA5X`q~' );
define( 'LOGGED_IN_KEY',    'ZqK2KiL[^V&C1..QZd<t^NRe/mqc/y}?R6f7Si3m4nW 5C_xY&^u+L%9Yz/96 4_' );
define( 'NONCE_KEY',        'd+Igubcnh[T3-hW./<fy+j<hfQo>XgyxKt9=u9U)!^eAU/7@zJC3s35M)yJh@)i^' );
define( 'AUTH_SALT',        '@XuYYlz$/*pGAJm>c`(i6F=@HJj~<)VsjtmhZ2JPs/]ALooatw+<*_#Xm$tOO:xR' );
define( 'SECURE_AUTH_SALT', 'qUAPpL5V?&jpOp@v#DT3uU*~7SWLyHkV%cK-jp?Ff9iA()T@F>O R4Czs;Tm]b^?' );
define( 'LOGGED_IN_SALT',   'Z$YFnJ&b2z9QG&D&-jd-$;g_y),#-A tF_z+/tyiD4NeB9k$$RqX#)( jJT&24$3' );
define( 'NONCE_SALT',       'EWW;X:3d_N^,^Q9nf[-ix!6XW27eZQ-QP]{QzIv[p<ap={rIpmQ$T{2|0V*~faj0' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );

define( 'WP_ALLOW_REPAIR', true );
